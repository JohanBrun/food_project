* Frontend

This projects contains a sample of minimal Single Page Application.

It uses the backend part from [[https://gitlab.com/fs2019/backend_food][backend_food]] project.

** Browserify

Pour vous faciliter la tâche, nous allons utiliser [[http://browserify.org/][browserify]], un outil qui
permet de gérer les dépendances et de produire un fichier unique pour l'exécuter
côté client.

#+BEGIN_SRC shell
npm install --save-dev browserify
# npm install -g browserify # for a global install
#+END_SRC

[[./package.json][package.json]] rajoute le script ~npm run build~ qui va appeler ~browserify~ pour
générer le fichier ~public/js/app.js~ à partir de ~main.js~.

Quel est l'intérêt ? Retrouver l'usage des modules comme dans ~node.js~:

#+BEGIN_SRC javascript
// main.js
const handlebars = require('handlebars')
#+END_SRC 

Avec cet outil, on gère de nouveau les modules avec ~npm~, même pour le côté
client !

Après chaque modification de ~main.js~, il faut donc maintenant faire

#+BEGIN_SRC shell
browserify main.js -o ./public/js/app.js
#+END_SRC

Cela est fastidieux, utilisons donc [[https://github.com/substack/watchify][watchify]] qui fait cela automatiquement.

#+BEGIN_SRC shell 
npm install --save-dev watchify
# or -g 
#watchify main.js -o ./public/js/app.js -v
npm run watch # see package.json scripts section
#+END_SRC

** Instructions pour tester

*** Côté backend

Récupérew le projet [[https://gitlab.com/fs2019/backend_food][backend_food]], suivez les instructions d'installation et de
démarrage.

Si tout se passe bien, la partie backend de l'application est maintenant en
écoute sur ~http://localhost:2999~.

Testew avec ~curl~ pour vérifier le bon fonctionnement. Mieux encore, utiliser
un client REST intégré à votre éditeur de code préféré.

*** Côté frontend

Il suffit d'ouvrir le fichier [[file:index.html][index.html]] dans un navigateur web.

L'entête du fichier référence le code javascript ~public/js/app.js~, qui est le
résultat par ~browserify~ du fichier [[file:main.js][main.js]].

#+BEGIN_SRC html
    <script src="public/js/app.js"></script>
#+END_SRC

À chaque changement de code dans ~main.js~, ~watchify~ va régénérer
~public/js/app.js~. Il reste encore à recharger la page dans le navigateur.

Pour faire cela automatiquement, j'ai mis en place [[https://www.browsersync.io/][browser-sync]].

#+BEGIN_SRC shell
npm install --save-dev browser-sync
#+END_SRC

Pour lancer en parallèle ~watchify~ et ~browser-sync~, j'ai crée le script ~npm
run watch~.
Les plus curieux peuvent consulter [[file:package.json][package.json]] dans la rubrique ~scripts~.

En pratique:

#+BEGIN_SRC shell
npm run watch
# watchify: watches main.js changes to regenerate public/js/app.js
# browser-sync: open browser, and reload page when main.js changes
#+END_SRC 

** Code frontend de base
Le code fourni donne un exemple d'utilisation de module:

[[file:main.js]] : 

#+begin_src javascript
console.log('main application loaded !')

const Food = require('./src/models/food')

console.log(Food.message)
#+end_src

[[file:src/models/food.js]] :
#+begin_src javascript
module.exports = {message: 'this is in food module'}

#+end_src

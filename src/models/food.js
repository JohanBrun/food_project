function Food(nameOrObject, weight, caloriesTotal, lipides, glucides, proteines){
  if (nameOrObject instanceof Object) {
    Object.assign(this, nameOrObject)
  } else {
    this.name = nameOrObject
    this.weight = weight
    this.caloriesTotal = caloriesTotal
    this.calories = () => this.lipides*9+this.glucides*4+this.proteines*4
    this.lipides = lipides
    this.glucides = glucides
    this.proteines = proteines
  }
}

Food.prototype.calories = function(){ return this.lipides*9+this.glucides*4+this.proteines*4}


Food.message = 'this is in food module'

module.exports = Food 

